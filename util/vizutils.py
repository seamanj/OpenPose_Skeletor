import getpass
from os.path import dirname

import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc
import torch
from PIL import Image

from util.imutils import rectangle_on_image, text_on_image, text_on_image_signbank

from util.openpose_related import *
from tqdm import tqdm
import errno
import os
import matplotlib as mpl

def mkdir_p(dir_path):
    try:
        os.makedirs(dir_path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def to_numpy(tensor):
    if torch.is_tensor(tensor):
        return tensor.cpu().numpy()
    elif type(tensor).__module__ != "numpy":
        raise ValueError(f"Cannot convert {type(tensor)} to numpy array")
    return tensor


def im_to_numpy(img):
    img = to_numpy(img)
    img = np.transpose(img, (1, 2, 0))  # H*W*C
    return img

def is_show(num_figs, iter_no, epoch_len):
    if num_figs == 0:
        return 0
    show_freq = int(epoch_len / num_figs)
    if show_freq != 0:
        return iter_no % show_freq == 0
    else:
        return 1


def _imshow_pytorch(rgb, ax=None):
    # from utils.transforms import im_to_numpy

    if not ax:
        fig = plt.figure()
        ax = fig.add_subplot(111)
    ax.imshow(im_to_numpy(rgb * 255).astype(np.uint8))
    ax.axis("off")


def fig2data(fig):
    """
    @brief Convert a Matplotlib figure to a 3D numpy array with RGB channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGB values
    """
    # draw the renderer
    fig.canvas.draw()
    w, h = fig.canvas.get_width_height()
    buf = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep="")
    buf = buf.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    return buf





def viz_gt_pred(
    thwc,
    inputs,
    masked_inputs,
    outputs,
    rgb_win,
    input_win,
    masked_input_win,
    output_win,
    fig,
    save_path=None,
    show=False,
):
    if save_path is not None:
        mkdir_p(dirname(save_path))

    suffix = ".avi"
    fourcc = cv2.VideoWriter_fourcc("M", "J", "P", "G")
    out = cv2.VideoWriter(str(save_path) + suffix, fourcc, 5, (1920, 1080))

    t, h, w, c = thwc.shape

    mpl.rcParams['text.color'] = 'w'
    mpl.rcParams.update({'font.size': 40})

    for i in tqdm(range(t)):
        rgb_img = thwc[i, :, :, :]
        input_img = draw_skeleton(inputs[i,:,:])
        masked_input_img = draw_skeleton(masked_inputs[i,:,:])
        output_img = draw_skeleton(outputs[i, :, :])
        if not fig:
            fig = plt.figure(figsize=(40, 20))
            fig.patch.set_facecolor('xkcd:black')
            ax1 = plt.subplot(141)
            ax1.title.set_text("Images")
            ax1.set_xticks([])
            ax1.set_yticks([])
            rgb_win = plt.imshow(rgb_img)
            ax2 = plt.subplot(142)
            ax2.title.set_text("Input")
            ax2.set_xticks([])
            ax2.set_yticks([])
            ax2.set_facecolor('xkcd:black')
            input_win = plt.imshow(input_img)
            ax3 = plt.subplot(143)
            ax3.set_facecolor('xkcd:black')
            ax3.title.set_text("conf > 0.5")
            ax3.set_xticks([])
            ax3.set_yticks([])
            masked_input_win = plt.imshow(masked_input_img)
            ax4 = plt.subplot(144)
            ax4.set_facecolor('xkcd:black')
            ax4.title.set_text("Output")
            ax4.set_xticks([])
            ax4.set_yticks([])
            output_win = plt.imshow(output_img)

        else:
            rgb_win.set_data(rgb_img)
            input_win.set_data(input_img)
            masked_input_win.set_data(masked_input_img)
            output_win.set_data(output_img)

        print()

        if show:
            print("Showing")
            plt.pause(0.05)

        fig_img = fig2data(fig)
        # fig_img = scipy.misc.imresize(fig_img, [1000, 1000])
        fig_img = np.array(Image.fromarray(fig_img).resize([1920, 1080]))
        out.write(fig_img[:, :, (2, 1, 0)])
    out.release()
    return rgb_win, input_win, masked_input_win, output_win, fig



def viz_gt_pred_without_image(
    inputs,
    outputs,
    input_win,
    output_win,
    fig,
    save_path=None,
    show=False,
):
    if save_path is not None:
        mkdir_p(dirname(save_path))

    suffix = ".avi"
    fourcc = cv2.VideoWriter_fourcc("M", "J", "P", "G")
    out = cv2.VideoWriter(str(save_path) + suffix, fourcc, 5, (1920, 1080))

    t = inputs.shape[0]

    for i in tqdm(range(t)):

        input_img = draw_skeleton(inputs[i,:,:])
        output_img = draw_skeleton(outputs[i, :, :])
        if not fig:
            fig = plt.figure(figsize=(40, 20))
            fig.patch.set_facecolor('xkcd:black')
            ax1 = plt.subplot(121)
            ax1.title.set_text("Input")
            ax1.set_xticks([])
            ax1.set_yticks([])
            ax1.set_facecolor('xkcd:black')
            input_win = plt.imshow(input_img)
            ax2 = plt.subplot(122)
            ax2.set_facecolor('xkcd:black')
            ax2.title.set_text("Output")
            ax2.set_xticks([])
            ax2.set_yticks([])
            output_win = plt.imshow(output_img)

        else:
            input_win.set_data(input_img)
            output_win.set_data(output_img)

        print()

        if show:
            print("Showing")
            plt.pause(0.05)

        fig_img = fig2data(fig)
        # fig_img = scipy.misc.imresize(fig_img, [1000, 1000])
        fig_img = np.array(Image.fromarray(fig_img).resize([1920, 1080]))
        out.write(fig_img[:, :, (2, 1, 0)])
    out.release()
    return input_win, output_win, fig
