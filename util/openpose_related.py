import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from src.skeletalModel import *
import numpy as np
import os
from os.path import *
from src.helpers import make_dir, embed_text
from glob import glob

# tj : headless mode
# https://stackoverflow.com/questions/15713279/calling-pylab-savefig-without-display-in-ipython
def fig2data(fig):
    """
    @brief Convert a Matplotlib figure to a 3D numpy array with RGB channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGB values
    """
    # draw the renderer
    fig.canvas.draw()
    w, h = fig.canvas.get_width_height()
    buf = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep="")
    buf = buf.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    return buf

def integrity_check(keypoints):
    keypoints = np.array(keypoints, dtype=float)
    keypoints = keypoints.reshape((-1, 3))
    pose_index = list(range(5,11)) + list(range(25,135))
    high_conf = keypoints[pose_index, 2] > 0.5
    valid = False
    valid_percent = sum(high_conf) / len(high_conf)
    if valid_percent > 0.5:
        valid = True
    return valid

def extract_skeleton(keypoints):
    keypoints = np.array(keypoints, dtype=float)
    keypoints = keypoints.reshape((-1, 3))
    pose_index = list(range(5,11)) + list(range(25,135))
    skeleton = keypoints[pose_index, :]
    return skeleton

def draw_openpose(keypoints):
    plt.ion()
    keypoints = np.array(keypoints, dtype=float)
    keypoints = keypoints.reshape((-1, 3))
    # keypoints_body = keypoints[5:11, :2]
    # keypoints_LH = keypoints[25:45, :2]
    # keypoints_RH = keypoints[45:65, :2]
    # keypoints_face = keypoints[65:, :2]

    fig = plt.figure(figsize=(7.2, 7.2))
    fig.patch.set_facecolor('xkcd:black')
    ax = fig.add_subplot(111)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_facecolor('xkcd:black')
    plt.gca().invert_yaxis() # tj : reverse Y, no need to reverse X
    limbs = getBody135SkeletalStructure()
    limbs = np.array(limbs)

    num_limbs = limbs.shape[0]

    # cmap = plt.get_cmap('rainbow')
    # colors = [cmap(i) for i in np.linspace(0, 1, num_limbs)]


    colors = [ # hands
              (100./255, 100./255, 100./255),
              (100./255, 0., 0.),
              (150/255., 0., 0.),
              (200/255., 0., 0.),
              (255/255., 0., 0.),
              (100/255., 100/255., 0.),
              (150/255., 150/255., 0.),
              (200/255., 200/255., 0.),
              (255/255., 255/255., 0.),
              (0., 100./255, 50./255),
              (0., 150./255, 75./255),
              (0., 200./255, 100./255),
              (0., 255./255, 125./255),
              (0., 50./255, 100./255),
              (0., 75./255, 150./255),
              (0., 100./255, 200./255),
              (0., 125./255, 255./255),
              (100./255, 0., 100./255),
              (150./255, 0., 150./255),
              (200./255, 0., 200./255),
              (255./255, 0., 255./255),
               # body
              (255. / 255, 0. / 255, 85. / 255),
              (170. / 255, 0. / 255, 255. / 255),
              (255. / 255, 0. / 255, 170. / 255),
              (85. / 255, 0. / 255, 255. / 255),
              (255. / 255, 0. / 255, 255. / 255),
              (170. / 255, 255. / 255, 0. / 255),
              (255. / 255, 85. / 255, 0. / 255),
              (85. / 255, 255. / 255, 0. / 255),
              (255. / 255, 170. / 255, 0. / 255),
              (0. / 255, 255. / 255, 0. / 255),
              (255. / 255, 255. / 255, 0. / 255),
              (0. / 255, 170. / 255, 255. / 255),
              (0. / 255, 255. / 255, 85. / 255),
              (0. / 255, 85. / 255, 255. / 255),
              (0. / 255, 255. / 255, 170. / 255),
              (0. / 255, 0. / 255, 255. / 255),
              (0. / 255, 255. / 255, 255. / 255),
              (255. / 255, 0. / 255, 0. / 255),
              (255. / 255, 0. / 255, 0. / 255),
              (0. / 255, 0. / 255, 255. / 255),
              (0. / 255, 0. / 255, 255. / 255),
              (0. / 255, 0. / 255, 255. / 255),
              (0. / 255, 255. / 255, 255. / 255),
              (0. / 255, 255. / 255, 255. / 255),
              (0. / 255, 255. / 255, 255. / 255),
              #face
              (1., 1., 1.)
              ]

    for i in range(num_limbs):
        start = limbs[i, 0]
        end = limbs[i, 1]
        color_idx = limbs[i, 2]
        if keypoints[start, 0] == 0 and keypoints[start, 1] == 0 or keypoints[end, 0] == 0 and keypoints[end, 1] == 0:
            pass
        else:
            ax.plot([keypoints[start, 0], keypoints[end, 0]], [keypoints[start, 1], keypoints[end, 1]],
                    color=colors[color_idx])
            ax.scatter(keypoints[start, 0], keypoints[start, 1], s=10, color = colors[color_idx])
            ax.scatter(keypoints[end, 0], keypoints[end, 1], s=10, color=colors[color_idx])


def draw_skeleton(skeleton):
    # plt.ion()

    fig = plt.figure(figsize=(7.2, 7.2))
    fig.patch.set_facecolor('xkcd:black')
    ax = fig.add_subplot(111)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_facecolor('xkcd:black')
    plt.gca().invert_yaxis() # tj : reverse Y, no need to reverse X
    limbs = get116SkeletalStructure()
    limbs = np.array(limbs)

    num_limbs = limbs.shape[0]

    # cmap = plt.get_cmap('rainbow')
    # colors = [cmap(i) for i in np.linspace(0, 1, num_limbs)]


    colors = [ # hands
              (100./255, 100./255, 100./255),
              (100./255, 0., 0.),
              (150/255., 0., 0.),
              (200/255., 0., 0.),
              (255/255., 0., 0.),
              (100/255., 100/255., 0.),
              (150/255., 150/255., 0.),
              (200/255., 200/255., 0.),
              (255/255., 255/255., 0.),
              (0., 100./255, 50./255),
              (0., 150./255, 75./255),
              (0., 200./255, 100./255),
              (0., 255./255, 125./255),
              (0., 50./255, 100./255),
              (0., 75./255, 150./255),
              (0., 100./255, 200./255),
              (0., 125./255, 255./255),
              (100./255, 0., 100./255),
              (150./255, 0., 150./255),
              (200./255, 0., 200./255),
              (255./255, 0., 255./255),
               # body
              (255. / 255, 0. / 255, 85. / 255),
              (170. / 255, 0. / 255, 255. / 255),
              (255. / 255, 0. / 255, 170. / 255),
              (85. / 255, 0. / 255, 255. / 255),
              (255. / 255, 0. / 255, 255. / 255),
              (170. / 255, 255. / 255, 0. / 255),
              (255. / 255, 85. / 255, 0. / 255),
              (85. / 255, 255. / 255, 0. / 255),
              (255. / 255, 170. / 255, 0. / 255),
              (0. / 255, 255. / 255, 0. / 255),
              (255. / 255, 255. / 255, 0. / 255),
              (0. / 255, 170. / 255, 255. / 255),
              (0. / 255, 255. / 255, 85. / 255),
              (0. / 255, 85. / 255, 255. / 255),
              (0. / 255, 255. / 255, 170. / 255),
              (0. / 255, 0. / 255, 255. / 255),
              (0. / 255, 255. / 255, 255. / 255),
              (255. / 255, 0. / 255, 0. / 255),
              (255. / 255, 0. / 255, 0. / 255),
              (0. / 255, 0. / 255, 255. / 255),
              (0. / 255, 0. / 255, 255. / 255),
              (0. / 255, 0. / 255, 255. / 255),
              (0. / 255, 255. / 255, 255. / 255),
              (0. / 255, 255. / 255, 255. / 255),
              (0. / 255, 255. / 255, 255. / 255),
              #face
              (1., 1., 1.)
              ]

    for i in range(num_limbs):
        start = limbs[i, 0]
        end = limbs[i, 1]
        color_idx = limbs[i, 2]
        if skeleton[start, 0] == 0 and skeleton[start, 1] == 0 or skeleton[end, 0] == 0 and skeleton[end, 1] == 0:
            pass
        else:
            ax.plot([skeleton[start, 0], skeleton[end, 0]], [skeleton[start, 1], skeleton[end, 1]], linewidth=5,
                    color=colors[color_idx])
            ax.scatter(skeleton[start, 0], skeleton[start, 1], s=30, color = colors[color_idx])
            ax.scatter(skeleton[end, 0], skeleton[end, 1], s=30, color=colors[color_idx])
    fig_img = fig2data(fig)
    plt.close()
    return fig_img