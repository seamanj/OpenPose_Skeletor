import cv2
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc
import scipy.ndimage
from PIL import Image, ImageDraw, ImageFont

from .misc import to_numpy, to_torch


def im_to_numpy(img):
    img = to_numpy(img)
    img = np.transpose(img, (1, 2, 0))  # H*W*C
    return img


def im_to_torch(img):
    img = np.transpose(img, (2, 0, 1))  # C*H*W
    img = to_torch(img).float()
    if img.max() > 1:
        img /= 255
    return img


def im_to_video(img):
    assert img.dim() == 3
    nframes = int(img.size(0) / 3)
    return img.contiguous().view(3, nframes, img.size(1), img.size(2))


def video_to_im(video):
    assert video.dim() == 4
    assert video.size(0) == 3
    return video.view(3 * video.size(1), video.size(2), video.size(3))


def load_image(img_path):
    # H x W x C => C x H x W
    return im_to_torch(scipy.misc.imread(img_path, mode="RGB"))


def resize(img, owidth, oheight):
    img = im_to_numpy(img)
    print(("%f %f" % (img.min(), img.max())))
    img = scipy.misc.imresize(img, (oheight, owidth))
    img = im_to_torch(img)
    print(("%f %f" % (img.min(), img.max())))
    return img


def resize_generic(img, oheight, owidth, interp="bilinear", is_flow=False):
    """
    Args
    inp: numpy array: RGB image (H, W, 3) | video with 3*nframes (H, W, 3*nframes)
          |  single channel image (H, W, 1) | -- not supported:  video with (nframes, 3, H, W)
    """

    # resized_image = cv2.resize(image, (100, 50))
    ht, wd, chn = img.shape[0], img.shape[1], img.shape[2]
    if chn == 1:
        resized_img = scipy.misc.imresize(
            img.squeeze(), [oheight, owidth], interp=interp, mode="F"
        ).reshape((oheight, owidth, chn))
    elif chn == 3:
        # resized_img = scipy.misc.imresize(img, [oheight, owidth], interp=interp)  # mode='F' gives an error for 3 channels
        resized_img = cv2.resize(img, (owidth, oheight))  # inverted compared to scipy
    elif chn == 2:
        # assert(is_flow)
        resized_img = np.zeros((oheight, owidth, chn), dtype=img.dtype)
        for t in range(chn):
            # resized_img[:, :, t] = scipy.misc.imresize(img[:, :, t], [oheight, owidth], interp=interp)
            # resized_img[:, :, t] = scipy.misc.imresize(img[:, :, t], [oheight, owidth], interp=interp, mode='F')
            # resized_img[:, :, t] = np.array(Image.fromarray(img[:, :, t]).resize([oheight, owidth]))
            resized_img[:, :, t] = scipy.ndimage.interpolation.zoom(
                img[:, :, t], [oheight, owidth]
            )
    else:
        in_chn = 5
        # Workaround, would be better to pass #frames
        if chn == 16:
            in_chn = 1
        if chn == 32:
            in_chn = 2
        nframes = int(chn / in_chn)
        img = img.reshape(img.shape[0], img.shape[1], in_chn, nframes)
        resized_img = np.zeros((oheight, owidth, in_chn, nframes), dtype=img.dtype)
        for t in range(nframes):
            frame = img[:, :, :, t]  # img[:, :, t*3:t*3+3]
            frame = cv2.resize(frame, (owidth, oheight)).reshape(
                oheight, owidth, in_chn
            )
            # frame = scipy.misc.imresize(frame, [oheight, owidth], interp=interp)
            resized_img[:, :, :, t] = frame
        resized_img = resized_img.reshape(
            resized_img.shape[0], resized_img.shape[1], chn
        )

    if is_flow:
        # print(oheight / ht)
        # print(owidth / wd)
        resized_img = resized_img * oheight / ht
    return resized_img


def imshow(img):
    npimg = im_to_numpy(img * 255).astype(np.uint8)
    plt.imshow(npimg)
    plt.axis("off")


def rectangle_on_image(img, width=5, frame_color="yellow"):
    img_pil = Image.fromarray(img)
    draw = ImageDraw.Draw(img_pil)
    cor = (0, 0, img_pil.size[0], img_pil.size[1])
    for i in range(width):
        draw.rectangle(cor, outline=frame_color)
        cor = (cor[0] + 1, cor[1] + 1, cor[2] - 1, cor[3] - 1)
    return np.asarray(img_pil)

def get_colors(inp, colormap, vmin=None, vmax=None):
    norm = plt.Normalize(vmin, vmax)
    return colormap(norm(inp))

def text_on_image_signbank(img, txt, classes, pos):
    x = 5
    y = 5
    img_pil = Image.fromarray(img)
    draw = ImageDraw.Draw(img_pil)
    # font_name = "FreeSerif.ttf"
    # font_name = "DejaVuSerif.ttf"
    font_name = "DejaVuSans-Bold.ttf"
    font = ImageFont.truetype(font_name, int(img.shape[0] / 20))
    w, h = font.getsize(txt)
    # if w - 2 * x > img.shape[0]:
    #     font = ImageFont.truetype(
    #         font_name, int(img.shape[0] * img.shape[0] / (8 * (w - 2 * x)))
    #     )
    #     w, h = font.getsize(txt)
    draw.rectangle((x, y, x + w, y + h), fill="black")
    draw.text((x, y), txt, fill=(255, 255, 255), font=font)

    draw.rectangle((30, 200, 30 + 180, 200 + 10), fill='white')

    colors = plt.cm.jet(np.linspace(0, 1, 1064))

    range_size = len(classes)
    for i in range(range_size):
        # if gt[i]:
        #     draw.rectangle(( 30 + i / range_size * 180, 180, 30+ (i+1) / range_size * 180, 180+10), fill='#ff0000')
        if classes[i] != -1:
            colorval = "#%02x%02x%02x" % (int(colors[classes[i]][0] * 255), int(colors[classes[i]][1] * 255), int(colors[classes[i]][2] * 255))
            draw.rectangle((30 + i / range_size * 180, 200, 30 + (i + 1) / range_size * 180, 200 + 10), fill=colorval)

    # tj : draw cursor
    draw.line((30 + pos / range_size * 180, 200, 30 + pos / range_size * 180, 210), fill=0, width=1)

    # x1 = 5
    # y1 = 200
    # pos_txt = 'pos' if pos else 'neg'
    # w1, h1 = font.getsize(pos_txt)
    # draw.rectangle((x1, y1, x1 + w1, y1 + h1), fill="black")
    # draw.text((x1, y1), pos_txt, fill=(255, 255, 255), font=font)


    return np.asarray(img_pil)


def text_on_image(img, pre, t):
    x = 5
    y = 5
    img_pil = Image.fromarray(img)
    draw = ImageDraw.Draw(img_pil)
    # font_name = "FreeSerif.ttf"
    # font_name = "DejaVuSerif.ttf"
    font_name = "DejaVuSans-Bold.ttf"
    font = ImageFont.truetype(font_name, int(img.shape[0] / 20))
    # w, h = font.getsize(txt)
    # # if w - 2 * x > img.shape[0]:
    # #     font = ImageFont.truetype(
    # #         font_name, int(img.shape[0] * img.shape[0] / (8 * (w - 2 * x)))
    # #     )
    # #     w, h = font.getsize(txt)
    # draw.rectangle((x, y, x + w, y + h), fill="black")
    # draw.text((x, y), txt, fill=(255, 255, 255), font=font)

    # tj : draw gt and pre
    # draw.text((5, 180), 'gt', fill=(255, 255, 255), font=font)
    # draw.rectangle((30, 180, 30+180, 180+10), fill='white')
    draw.text((5, 200), 'pre', fill=(255, 255, 255), font=font)
    draw.rectangle((30, 200, 30+180, 200+10), fill='white')
    range_size = 63
    line_points = []
    for i in range(range_size):
        # if gt[i]:
        #     draw.rectangle(( 30 + i / range_size * 180, 180, 30+ (i+1) / range_size * 180, 180+10), fill='#ff0000')
        if pre[i] > 0.5:
            draw.rectangle((30 + i / range_size * 180, 200, 30 + (i + 1) / range_size * 180, 200 + 10), fill='#00ff00')
        # tj : draw pre curve
        line_points.append((30 + i / range_size * 180, 200 + (1-pre[i]) * 10))
        # draw.line( (30 + i / range_size * 180, 200 + (1-pre[i]) * 10, 30 + (i + 1) / range_size * 180, 200 + (1-pre[i+1]) * 10), fill=0, width=1)
    i += 1
    line_points.append((30 + i / range_size * 180, 200 + (1 - pre[i]) * 10))
    draw.line(line_points, width=1, fill=0, joint='curve')

    # tj : draw cursor
    draw.line((30 + t / range_size * 180, 180, 30 + t / range_size * 180, 210), fill=0, width=1)


    return np.asarray(img_pil)
