
import argparse
import os
import pickle
import gzip
from tqdm import tqdm


def make_dir(dir: str) -> str:
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir


def load_gklz(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object


def save_gklz(obj, filename):
    with gzip.open(filename, "wb") as f:
        pickle.dump(obj, f, -1)


def main(params):
    data_roots = params.data_roots.split(',')
    output_root = params.output_root
    train_f = open(os.path.join(output_root, "train.txt"), "w")
    val_f = open(os.path.join(output_root, "val.txt"), "w")
    test_f = open(os.path.join(output_root, 'test.txt'), 'w')
    count = 0
    for data_root in data_roots:
        openpose_files = [
            os.path.join(root, name)
            for root, _, files in os.walk(data_root)
            for name in files
            if name.endswith('.tar.xz')
        ]
        for openpose_file in tqdm(openpose_files):
            count += 1
            if count % 6 == 4: # val
                val_f.write(openpose_file + '\n')
                print("{} is categorized to val".format(openpose_file))
            elif count % 6 == 5: # test
                test_f.write(openpose_file + '\n')
                print("{} is categorized to test".format(openpose_file))
            else:
                train_f.write(openpose_file + '\n')
                print("{} is categorized to train".format(openpose_file))



def load_data():
    a = load_gklz("/vol/research/SignPose/tj/dataset/DCAL_Czech/Interview/Glasgow/9+10/G10i_Czech.gklz")
    print('1')


if __name__ == "__main__":
    # load_data()

    # Assumes they are in the same order
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--data_roots",
        type=str,
        default="/vol/research/SignTranslation/data/SWISSTXT/SWISSTXT-WEATHER-RAW/OpenPose_r704,"
                "/vol/research/SignTranslation/data/SWISSTXT/SWISSTXT-NEWS-RAW/OpenPose_r704",
    )

    parser.add_argument(
        "--output_root",
        type=str,
        default="/vol/research/SignTranslation/tmp/tj0012/datasets/SWISSTXT/splits",
    )
    params, _ = parser.parse_known_args()
    main(params)
