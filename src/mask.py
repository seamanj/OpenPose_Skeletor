import numpy as np
import random
import torch
from torch import Tensor


def shuffle_2D_in_rows(arr):
    x, y = arr.shape
    rows = np.indices((x, y))[1]
    cols = [np.random.permutation(y) for _ in range(x)]
    return arr[:, cols]


#  tj : bit 1 means mask, means
def gen_mask(batch_size: int, window_size: int, mask_percentage: float, conf, with_sequence_ordering = False) -> Tensor:

        assert mask_percentage >= 0. and mask_percentage <= 1., \
            "mask_percentage should be between 0 and 1."

        if conf is not None and mask_percentage != 0:
            num_masked = int(mask_percentage * window_size)
            num_conf_masked = int(mask_percentage * 0.666666667 * window_size)
            num_random_masked = num_masked - num_conf_masked

            conf_ave = torch.mean(conf, dim=2)
            sorted, indices = torch.sort(conf_ave, 1) # tj : dim 0 means along downward axis, dim 1 means along right axis
            conf_mask_indices = indices[:, :num_conf_masked]

            # tj : https://discuss.pytorch.org/t/take-random-sample-of-long-tensor-create-new-subset/36244/3
            conf_unmask_indices = indices[:, num_conf_masked:]
            num_conf_unmasked = conf_unmask_indices.shape[1]
            rand_indices = [torch.randperm(num_conf_unmasked)[:num_random_masked] for _ in range(batch_size)] #  tj : randperm : from 0 to n-1
            # = tj : we permute each row
            # = tj : permute first, then take the first 'num_random_masked' elements
            rand_indices = torch.stack(rand_indices, dim=0)
            # = tj : stack the list into an array
            rand_mask_indices = torch.gather(conf_unmask_indices, 1, rand_indices)
            # = tj : choose the indices randomly from the masks not covered by confident mask
            # + tj : first get list, and then stack.
            # https://stackoverflow.com/questions/57917981/pytorch-how-to-stack-tensor-like-for-loop
            mask_indices = torch.cat((conf_mask_indices, rand_mask_indices), 1)
            mask = torch.zeros((batch_size, window_size)).scatter(1, mask_indices, torch.ones((mask_indices.shape)))
            # print('1')
            mask = ( mask != 0)
            if with_sequence_ordering:
                mask = torch.cat((torch.tensor(False).repeat(mask.shape[0],1), mask), dim=1)  # shape : (64, 33)
            # print('1')
        else:
            mask = np.full((batch_size,window_size), False)
            num_masked = int(mask_percentage * window_size)
            mask[:,:num_masked] = True
            [random.shuffle(mask[i,:]) for i in range(batch_size)]
            if with_sequence_ordering:
                mask = np.concatenate((np.full((batch_size,1),False), mask), axis=1) # = tj : the first is the cls specular token
            mask = torch.from_numpy(mask) # tj : convert to tensor

        return mask


def gen_mask_option(batch_size: int, window_size: int, mask_percentage: float, conf, with_sequence_ordering=False, option=None) -> Tensor:
    assert mask_percentage >= 0. and mask_percentage <= 1., \
        "mask_percentage should be between 0 and 1."

    if mask_percentage == 0:
        mask = torch.zeros((batch_size, window_size), dtype=torch.uint8)
    else:
        if option == 'highest' or option == 'lowest':
            num_masked = int(mask_percentage * window_size)
            conf_ave = torch.mean(conf, dim=2)

            if option == 'highest':
                sorted, indices = torch.sort(conf_ave, 1,
                                             descending=True)  # tj : dim 0 means along downward axis, dim 1 means along right axis
            elif option == 'lowest':
                sorted, indices = torch.sort(conf_ave, 1, descending=False)
            mask_indices = indices[:, :num_masked]
            mask = torch.zeros((batch_size, window_size)).scatter(1, mask_indices, torch.ones((mask_indices.shape)))
            # print('1')
            mask = (mask != 0)
            if with_sequence_ordering:
                mask = torch.cat((torch.tensor(False).repeat(mask.shape[0], 1), mask), dim=1)  # shape : (64, 33)
        elif option == 'random':
            mask = np.full((batch_size, window_size), False)
            num_masked = int(mask_percentage * window_size)
            mask[:, :num_masked] = True
            [random.shuffle(mask[i, :]) for i in range(batch_size)]
            if with_sequence_ordering:
                mask = np.concatenate((np.full((batch_size, 1), False), mask),
                                      axis=1)  # = tj : the first is the cls specular token
            mask = torch.from_numpy(mask)  # tj : convert to tensor
        else:
            raise Exception("incorrect option used!")
    return mask



def gen_joint_mask_option(batch_size: int, window_size: int, mask_percentage: float, conf, option=None) -> Tensor:
    assert mask_percentage >= 0. and mask_percentage <= 1., \
        "mask_percentage should be between 0 and 1."

    if mask_percentage != 0 and option == 'highest' or option == 'lowest':
        joint_size = conf.shape[2]
        num_joint_masked = int(mask_percentage * window_size * joint_size)

        if option == 'highest':
            sorted, indices = torch.sort(conf.view(batch_size, -1), 1, descending=True)  # tj : dim 0 means along downward axis, dim 1 means along right axis
        elif option == 'lowest':
            sorted, indices = torch.sort(conf.view(batch_size, -1), 1, descending=False)
        mask_indices = indices[:, :num_joint_masked]
        
        mask_frame_indices = mask_indices / joint_size
        mask_joint_indices = mask_indices % joint_size

        mask = torch.zeros((batch_size, window_size * joint_size)).scatter(1, mask_indices, torch.ones((mask_indices.shape)))
        mask = mask.view((-1, window_size, joint_size))
        # print('1')
        mask = (mask != 0)
        # print('1')
    elif option == 'random':
        joint_size = conf.shape[2]
        mask = np.full((batch_size, window_size * joint_size), False)
        num_joint_masked = int(mask_percentage * window_size * joint_size)
        mask[:, :num_joint_masked] = True
        [random.shuffle(mask[i, :]) for i in range(batch_size)]
        mask = mask.reshape((-1, window_size, joint_size))
        mask = torch.from_numpy(mask)  # tj : convert to tensor
    else:
        raise Exception(" unsupported option!")
    return mask


def gen_train_mask(joint_mask, conf, mask_percentage, option) -> Tensor:
    assert mask_percentage >= 0. and mask_percentage <= 1., \
        "mask_percentage should be between 0 and 1."

    train_mask = np.zeros_like(joint_mask)
    # = tj : https://numpy.org/doc/stable/reference/generated/numpy.where.html
    masked_conf = np.where(joint_mask, conf, 0) #  value = conf if joint_mask = True else 0
    num_total = np.count_nonzero(joint_mask)
    num_masked = int(num_total * mask_percentage)
    #https://stackoverflow.com/questions/46554647/returning-the-n-largest-values-indices-in-a-multidimensional-array-can-find-so
    # get the largest n number in a 2d array

    if option == 'highest':
        masked_indices = np.argsort(masked_conf.ravel())[-num_masked:][::-1]
        row_col = np.c_[np.unravel_index(masked_indices, masked_conf.shape)]
        train_mask[row_col[:, 0], row_col[:, 1]] = 1
    elif option == 'random':
        row_col = np.nonzero(joint_mask)
        selected_indices = random.sample(range(0, num_total), num_masked)
        train_mask[row_col[0][selected_indices], row_col[1][selected_indices]] = 1
    else:
        raise Exception(" unsupported option!")
    return train_mask

