import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from src.skeletalModel import getSkeletalModelStructure, getMTCSkeletalModelStructure
import numpy as np
import os
from os.path import *
from src.helpers import make_dir, embed_text
from glob import glob

# tj : headless mode
# https://stackoverflow.com/questions/15713279/calling-pylab-savefig-without-display-in-ipython

save_fig = True

def drawJoints(Yx, Yy, Yz, foldName=None, type=None, root_dir=None, mask=None):
    plt.ion()
    #plt.show()
    fig = plt.figure(figsize=(7.2, 7.2))

    save_folder = ''
    ax = fig.add_subplot(111,projection='3d')
    if type == 'openpose':
        ax.view_init(-90, -90) # tj 90, 90 front
        save_folder = join(root_dir, foldName)
        make_dir(save_folder)
        skeleton = getSkeletalModelStructure()
    else:
        raise Exception("Nothing else supported yet!")

    T, n = Yx.shape

    skeleton = np.array(skeleton)

    number = skeleton.shape[0]
    cmap = plt.get_cmap('rainbow')
    colors = [cmap(i) for i in np.linspace(0, 1, number)]
    plt.axis('off')

    for i in range(T):
        # for j in range(n):
        #      #print(Yx[i,j], Yy[i,j], Yz[i,j])
        #      ax.scatter(Yx[i,j], Yy[i,j], Yz[i,j], c = 'b', marker = 'o')
        # text(x, y, s, fontsize=12)
        if mask is not None:
            if mask[i].any(): # tj : change the background color when the frame is masked
                ax.text2D(0.5, 0.9, "▨", fontsize=24, transform=ax.transAxes)
                # fig.patch.set_facecolor('black') # tj : outer background
                # ax.set_facecolor("black") # tj : inner background
            # else: # tj : otherwise, change it back
            #     fig.patch.set_facecolor('white') # tj : outer background
            #     ax.set_facecolor("white") # tj : inner background


        for j in range(number):
            if Yx[i,skeleton[j,0]] == 0 and Yy[i,skeleton[j,0]] == 0 or Yx[i,skeleton[j,1]] == 0 and Yy[i,skeleton[j,1]] == 0:
                pass
            else:
                ax.plot([Yx[i,skeleton[j,0]], Yx[i,skeleton[j,1]]], [Yy[i,skeleton[j,0]], Yy[i,skeleton[j,1]]], [0,0], color=colors[j])


        #plt.draw()
        if save_fig:
            filename = join(save_folder, "skeleton_3d_frame%d.png" % i)
            # https://stackoverflow.com/questions/4804005/matplotlib-figure-facecolor-background-color
            plt.savefig(filename, dpi=100, facecolor=fig.get_facecolor(), edgecolor='none')
        # plt.show()
        # plt.pause(0.3)
        for j in range(number):
            if Yx[i,skeleton[j,0]] == 0 and Yy[i,skeleton[j,0]] == 0 or Yx[i,skeleton[j,1]] == 0 and Yy[i,skeleton[j,1]] == 0:
                pass
            else:
                ax.lines.pop(0)

        if mask is not None:
            if mask[i].any(): # tj : change the background color when the frame is masked
                ax.texts.pop(0)

        # plt.clf()
    video_name = join(save_folder, foldName + '.mp4')
    gen_video_command = 'ffmpeg -y -i ' + save_folder + '/skeleton_3d_frame%d.png -c:v libx264 ' + video_name
    print(gen_video_command)
    os.system(gen_video_command)

def draw2DJoints( Yx, Yy, foldName, root_dir, name):
    plt.ion()
    #plt.show()
    fig = plt.figure(figsize=(7.2, 7.2))

    ax = fig.add_subplot(111,projection='3d')
    ax.view_init(-90, -90)
    save_folder = join(root_dir, foldName)
    skeleton = getSkeletalModelStructure()


    make_dir(save_folder)
    skeleton = np.array(skeleton)

    number = skeleton.shape[0]
    cmap = plt.get_cmap('rainbow')
    colors = [cmap(i) for i in np.linspace(0, 1, number)]
    plt.axis('off')

    num_people = Yx.shape[0]

    for i in range(1):  # tj : manually choose the first person
        # for j in range(n):
        #      #print(Yx[i,j], Yy[i,j], Yz[i,j])
        #      ax.scatter(Yx[i,j], Yy[i,j], Yz[i,j], c = 'b', marker = 'o')

        for j in range(number):
            # print("%d - %d"%(skeleton[j,0], skeleton[j,1]))
            # print("%f, %f - %f, %f"%(Yx[i,skeleton[j,0]], Yx[i,skeleton[j,1]], Yy[i,skeleton[j,0]], Yy[i,skeleton[j,1]]))
            if Yx[i,skeleton[j,0]] == 0 and Yy[i,skeleton[j,0]] == 0 or Yx[i,skeleton[j,1]] == 0 and Yy[i,skeleton[j,1]] == 0:
                pass
            else:
                ax.plot([Yx[i,skeleton[j,0]], Yx[i,skeleton[j,1]]], [Yy[i,skeleton[j,0]], Yy[i,skeleton[j,1]]], [0,0], color=colors[j])
        #plt.draw()
    if save_fig:
        plt.savefig(join(save_folder, name), dpi=100)
        #plt.show()
        #plt.pause(0.3)
        # for j in range(number):
        #     ax.lines.pop(0)
        #plt.clf()


def drawPlain( foldName, root_dir, name):
    plt.ion()
    #plt.show()
    fig = plt.figure(figsize=(7.2, 7.2))

    ax = fig.add_subplot(111,projection='3d')
    save_folder = join(root_dir, foldName)
    make_dir(save_folder)
    plt.axis('off')
    if save_fig:
        plt.savefig(join(save_folder, name), dpi=100)
        #plt.show()
        #plt.pause(0.3)
        # for j in range(number):
        #     ax.lines.pop(0)
        #plt.clf()






def draw(root_dir:str, headless:bool=False ):

    if headless:
        matplotlib.use('Agg')

    origin = np.loadtxt(join(root_dir, "origin.txt"))
    origin_x = origin[0:origin.shape[0], 0:origin.shape[1]:3]
    origin_y = origin[0:origin.shape[0], 1:origin.shape[1]:3]
    origin_z = origin[0:origin.shape[0], 2:origin.shape[1]:3]
    drawJoints(origin_x, origin_y, origin_z, foldName="origin", type="openpose", root_dir=root_dir)

    mask = np.loadtxt(join(root_dir, "mask.txt"))


    predict = np.loadtxt(join(root_dir, "predict.txt"))
    predict_x = predict[0:predict.shape[0], 0:predict.shape[1]:3]
    predict_y = predict[0:predict.shape[0], 1:predict.shape[1]:3]
    predict_z = predict[0:predict.shape[0], 2:predict.shape[1]:3]
    drawJoints(predict_x, predict_y, predict_z, foldName="predict", type="openpose", root_dir=root_dir, mask=mask)

    masked_src = np.loadtxt(join(root_dir, "masked_src.txt"))
    masked_src_x = masked_src[0:masked_src.shape[0], 0:masked_src.shape[1]:3]
    masked_src_y = masked_src[0:masked_src.shape[0], 1:masked_src.shape[1]:3]
    masked_src_z = masked_src[0:masked_src.shape[0], 2:masked_src.shape[1]:3]
    drawJoints(masked_src_x, masked_src_y, masked_src_z, foldName="masked_src", type="openpose", root_dir=root_dir)


    origin_video_name = join(root_dir, 'origin', 'origin.mp4')
    origin_video_name_text = join(root_dir, 'origin', 'origin_text.mp4')
    predict_video_name = join(root_dir, 'predict', 'predict.mp4')
    predict_video_name_text = join(root_dir, 'predict', 'predict_text.mp4')


    masked_src_video_name = join(root_dir, 'masked_src', 'masked_src.mp4')
    masked_src_video_name_text = join(root_dir, 'masked_src', 'masked_src_text.mp4')


    merged_2_video_name = join(root_dir, 'merged_2.mp4')
    merged_3_video_name = join(root_dir, 'merged_3.mp4')

    embed_text(origin_video_name, origin_video_name_text, 'Origin')
    embed_text(predict_video_name, predict_video_name_text, 'Skeletor')
    embed_text(masked_src_video_name, masked_src_video_name_text, 'Noise')



    merge_2_command = 'ffmpeg -y -i ' + masked_src_video_name_text + ' -i ' + predict_video_name_text + \
                    ' -filter_complex "hstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_2_video_name
    #print(merge_command)
    os.system(merge_2_command)

    merge_3_command = 'ffmpeg -y -i ' + origin_video_name_text + ' -i ' + merged_2_video_name + \
                    ' -filter_complex "hstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_3_video_name
    #print(merge_command)
    os.system(merge_3_command)


    merged_fold = make_dir(join(root_dir, "merged"))
    extract_command = 'ffmpeg -i ' + merged_3_video_name + ' ' + merged_fold + '/%08d.png'
    os.system(extract_command)


def merge(left_dir:str, right_dir:str):
    left_video_name = join(left_dir, 'left.mp4')
    left_video_name_text = join(left_dir, 'left_text.mp4')
    gen_video_command = 'ffmpeg -y -i ' + left_dir + '/%08d.png -c:v libx264 ' + left_video_name
    print(gen_video_command)
    os.system(gen_video_command)

    right_video_name = join(right_dir, 'right.mp4')
    right_video_name_text = join(right_dir, 'right_text.mp4')
    gen_video_command = 'ffmpeg -y -i ' + right_dir + '/skeleton_3d_frame%d.png -c:v libx264 ' + right_video_name
    print(gen_video_command)
    os.system(gen_video_command)

    embed_text(left_video_name, left_video_name_text, 'Czech')
    embed_text(right_video_name, right_video_name_text, 'Skeletor')

    merged_video_name = join(left_dir, 'merged.mp4')

    merge_command = 'ffmpeg -y -i ' + left_video_name_text + ' -i ' + right_video_name_text + \
                    ' -filter_complex "hstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_video_name
    #print(merge_command)
    os.system(merge_command)







if __name__ == "__main__":


    # = tj : merge video
    merge('/vol/research/SignPose/tj/Workspace/Pheonix_Czech/results/dev/01April_2010_Thursday_heute-6698/images',
          '/home/seamanj/Workspace/BERT_skeleton/results/e_10_m_1000_lr_1e-3_w_32/eval/dev/01April_2010_Thursday_heute-6698/predict')




    # = tj : draw specified dir
    # dir = '/home/seamanj/Workspace/BERT_skeleton/results/e_1_m_100_lr_1e-3_t_2000_w_16/dev/57'
    # draw(root_dir=dir)

    # = tj : down-most dirs in one dir
    # dir_to_draw = '/home/seamanj/Workspace/BERT_skeleton/results/'
    # for dirpath, dirnames, filenames in os.walk(dir_to_draw):
    #     if not dirnames:
    #         print(dirpath)


    # = tj : specified deep-level dirs
    # dir_to_draw = '/home/seamanj/Workspace/BERT_skeleton/results/e_10_m_1000_lr_1e-3_w_32/*/*/'
    # for dir in glob(dir_to_draw):
    #     print(dir)
    #     draw(root_dir=dir)




    # origin = np.loadtxt("/home/seamanj/Workspace/BERT_skeleton/results/origin.txt")
    # origin_x = origin[0:origin.shape[0], 0:origin.shape[1]:3]
    # origin_y = origin[0:origin.shape[0], 1:origin.shape[1]:3]
    # origin_z = origin[0:origin.shape[0], 2:origin.shape[1]:3]
    # drawJoints(origin_x, origin_y, origin_z, foldName="origin", type="openpose")
    #
    #
    # predict = np.loadtxt("/home/seamanj/Workspace/BERT_skeleton/results/predict.txt")
    # predict_x = predict[0:predict.shape[0], 0:predict.shape[1]:3]
    # predict_y = predict[0:predict.shape[0], 1:predict.shape[1]:3]
    # predict_z = predict[0:predict.shape[0], 2:predict.shape[1]:3]
    # drawJoints(predict_x, predict_y, predict_z, foldName="predict", type="openpose")




