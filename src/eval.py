# coding: utf-8
"""
This modules holds methods for generating predictions from a model.
"""
import os
from os.path import *
import sys
from typing import List, Optional
from logging import Logger
import numpy as np
from pathlib import Path

from src.model import build_model

from src.data import *
from util.vizutils import *
import pickle


def save_gklz(obj, filename):
    with gzip.open(filename, "wb") as f:
        pickle.dump(obj, f, -1)

def load_gklz(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object

def evaluate(cfg_file,
         ckpt:str,
         logger:Logger=None) -> None:

    cfg = load_config(cfg_file)

    set_seed(seed=cfg["training"].get("random_seed", 42))

    eval_batch_size = cfg['evaluating']['batch_size']

    eval_dir = cfg['evaluating']['eval_dir']


    eval_data = load_eval_data(data_cfg=cfg["data"])


    model = build_model(cfg["model"], eval_data.skeleton_dim)  # tj : build our model
    model = model.cuda()

    # model = torch.nn.DataParallel(model).cuda()



    checkpoint = torch.load(ckpt)
    model.load_state_dict(checkpoint["model_state"])
    model.eval()

    eval_loader = make_data_loader(dataset=eval_data,
                                         batch_size=eval_batch_size,
                                         shuffle=False)


    input_win, output_win, fig = None, None, None


    inputs = []
    outputs = []
    for i, batch in enumerate(eval_loader):
        if batch is None:  # tj : filter out None in my_collate
            continue

        with torch.no_grad():

            skeleton = batch['skeleton']

            skeleton_cuda = skeleton.cuda()

            outputs_cuda = model(skeleton_cuda, train_mask=None)

            inputs.append(batch['skeleton'].squeeze(0).detach().numpy())
            outputs.append(outputs_cuda.squeeze(0).cpu().detach().numpy())

    inputs = np.vstack(inputs)
    outputs = np.vstack(outputs)


    # num_figs = 20
    # if is_show(num_figs, i, len(valid_loader)):
    fname = batch['name'][0]



    save_skeleton_dir = os.path.join(eval_dir, "skeleton")
    save_skeleton_path = abspath(os.path.join(save_skeleton_dir, fname + '.gklz'))
    mkdir_p(dirname(save_skeleton_path))
    save_gklz(outputs, save_skeleton_path)
    print('{} is saved'.format(save_skeleton_path))

    # a = load_gklz('/home/seamanj/Workspace/OpenPose_Skeletor/eval/exp1/skeleton/Michaela_Signing_I.gklz')

    save_fig_dir = os.path.join(eval_dir, "figs")
    save_path = os.path.join(save_fig_dir, fname)

    input_win, output_win, fig = viz_gt_pred_without_image(
        inputs,
        outputs,
        input_win,
        output_win,
        fig,
        save_path=save_path,
        show=False,
    )

    print()