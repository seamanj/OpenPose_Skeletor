import torch
from torch.utils.data import Dataset, DataLoader
import os
from glob import glob
import json
from src.helpers import *
from util.openpose_related import *
from src.mask import *
from src.my_collate import *
import cv2

def make_data_loader(dataset: Dataset,
                     batch_size: int,
                     shuffle: bool = False) -> DataLoader:
    params = {
        'batch_size': batch_size,
        'shuffle': shuffle,
        'num_workers': 4,
        'collate_fn': my_collate}
    return DataLoader(dataset, **params)



def load_train_data(data_cfg: dict) -> (Dataset):
    # = tj : print the train path
    train_path = data_cfg.get("train_data_path")
    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")
    down_sample_rate = data_cfg.get("down_sample_rate")
    window_valid_thres = data_cfg.get("window_valid_thres")
    train_mask_percentage = data_cfg.get("train_mask_percentage")


    return OpenPoseDataset(train_path, window_size, window_stride, window_valid_thres, train_mask_percentage)

def load_val_data(data_cfg: dict) -> (Dataset):
    # = tj : print the train path
    val_path = data_cfg.get("val_data_path")
    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")
    down_sample_rate = data_cfg.get("down_sample_rate")
    window_valid_thres = data_cfg.get("window_valid_thres")
    train_mask_percentage = data_cfg.get("train_mask_percentage")

    return OpenPoseDataset(val_path, window_size, window_stride, window_valid_thres, train_mask_percentage)


def load_test_data(data_cfg: dict) -> (Dataset):
    # = tj : print the train path
    test_path = data_cfg.get("test_data_path")
    window_size = data_cfg.get("window_size")
    window_stride = window_size
    down_sample_rate = data_cfg.get("down_sample_rate")
    window_valid_thres = data_cfg.get("window_valid_thres")
    train_mask_percentage = data_cfg.get("train_mask_percentage")

    return OpenPoseDataset(test_path, window_size, window_stride, window_valid_thres, train_mask_percentage, with_images=True)

def load_eval_data(data_cfg: dict) -> (Dataset):
    # = tj : print the train path
    eval_path = data_cfg.get("eval_data_path")
    window_size = data_cfg.get("window_size")
    window_stride = window_size
    down_sample_rate = data_cfg.get("down_sample_rate")
    window_valid_thres = data_cfg.get("window_valid_thres")

    return EvalDataset(eval_path, window_size, window_stride, window_valid_thres)


class OpenPoseDataset(Dataset):
    def __init__(self, data_path, window_size, window_stride, window_valid_thres, train_mask_percentage, with_images=False):
        self.window_size = window_size
        self.window_stride = window_stride
        self.window_valid_thres = window_valid_thres
        self.train_mask_percentage = train_mask_percentage

        self.samples = []  # tj : list,
        self.skeleton = {}  # tj : dict
        self.conf = {}
        self.with_images = with_images

        openpose_f = open(data_path, 'r')
        openpose_files = openpose_f.readlines()

        self.skeleton_dim = 0
        for openpose_file in openpose_files:
            print("read {}".format(openpose_file))
            openpose_file = openpose_file.strip()
            keypoints = load_openpose_tar_xz(openpose_file)

            num_frames = len(keypoints)
            valid = np.zeros((num_frames), dtype=np.int)
            # draw_openpose(keypoints[7689]["people"][0]["pose_keypoints_2d"])
            skeleton_conf = np.zeros((num_frames, 116, 3), dtype=np.float32) # tj : body 6 + hand 20 * 2 + face 70
            for i in range(num_frames):
                num_people = len(keypoints[i]["people"])
                if num_people == 0:
                    continue
                elif num_people > 1:
                    dist_min = float('inf')
                    x_left_shoulder_most_left = keypoints[i]["people"][0]["pose_keypoints_2d"][5*3]
                    # tj : 5 * 3 is the x position of the left shoulder
                    # = tj : the second [0] for x coordinate
                    idx_people = 0
                    for j in range(1, num_people):
                        x_left_shoulder_curr = keypoints[i]["people"][j]["pose_keypoints_2d"][5*3]
                        if x_left_shoulder_curr < x_left_shoulder_most_left:
                            x_left_shoulder_most_left = x_left_shoulder_curr
                            idx_people = j
                else:
                    idx_people = 0
                    # draw_openpose(keypoints[i]["people"][0]["pose_keypoints_2d"])
                if integrity_check(keypoints[i]["people"][idx_people]["pose_keypoints_2d"]):
                    valid[i] = 1
                skeleton_conf[i, :, :] = extract_skeleton(keypoints[i]["people"][idx_people]["pose_keypoints_2d"])

            skeleton = skeleton_conf[:, :, :2]
            conf = skeleton_conf[:, :, 2]

            self.skeleton[openpose_file] = skeleton
            self.conf[openpose_file] = conf
            self.skeleton_dim = 116 * 2
            i = 0
            while i < num_frames - window_size:
                if sum(valid[i:i+window_size]) / window_size >= window_valid_thres: # window valid percentage
                    self.samples.append({'openpose_path': openpose_file,
                                         'start_frame': i})
                    i += window_stride
                else:
                    i += 1

        print('1')

    def __len__(self):
        """
        Return the total number of samples
        """
        return len(self.samples)

    def __getitem__(self, index):
        """
        Generate one sample of the dataset
        """
        sample = {}

        sample = self.samples[index]
        openpose_path = sample['openpose_path']
        start_frame = sample['start_frame']
        if openpose_path not in self.skeleton:
            return None

        if self.with_images:
            video_path = openpose_path.replace('OpenPose_r704', 'Videos').replace('openpose.tar.xz', 'mp4')
            vidcap = cv2.VideoCapture(video_path)
            video_res_w = int(vidcap.get(cv2.CAP_PROP_FRAME_WIDTH))
            video_res_h = int(vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT))

            thwc = np.empty([self.window_size, video_res_h, video_res_w, 3], dtype=int)

            vidcap.set(cv2.CAP_PROP_POS_FRAMES, start_frame)
            for i, _ in enumerate(range(self.window_size)):
                success, bgr = vidcap.read()
                # BGR (OpenCV) to RGB (Torch)
                if success:
                    rgb = bgr[:, :, [2, 1, 0]]
                    # chw = np.transpose(rgb, (2, 0, 1))  # C*H*W
                    thwc[i, :, :, :] = rgb

            sample['thwc'] = thwc
        skeleton = self.skeleton[openpose_path][start_frame:start_frame + self.window_size]
        sample['skeleton'] = skeleton # x, y


        conf = self.conf[openpose_path][start_frame:start_frame + self.window_size]
        joint_mask = (conf > 0.5) * 1 # conf
        sample['joint_mask'] = joint_mask

        train_mask = gen_train_mask(joint_mask, conf, self.train_mask_percentage, option='random')
        sample['train_mask'] = train_mask

        return sample




class EvalDataset(Dataset):
    def __init__(self, openpose_file, window_size, window_stride, window_valid_thres, with_images=False):
        self.window_size = window_size
        self.window_stride = window_stride
        self.window_valid_thres = window_valid_thres

        self.samples = []  # tj : list,
        self.skeleton = {}  # tj : dict
        self.conf = {}
        self.with_images = with_images


        self.skeleton_dim = 0
        print("read {}".format(openpose_file))
        openpose_file = openpose_file.strip()
        keypoints = load_openpose_tar_xz(openpose_file)

        num_frames = len(keypoints)
        # draw_openpose(keypoints[7689]["people"][0]["pose_keypoints_2d"])
        skeleton_conf = np.zeros((num_frames, 116, 3), dtype=np.float32) # tj : body 6 + hand 20 * 2 + face 70
        for i in range(num_frames):
            num_people = len(keypoints[i]["people"])
            if num_people == 0:
                continue
            elif num_people > 1:
                dist_min = float('inf')
                x_left_shoulder_most_left = keypoints[i]["people"][0]["pose_keypoints_2d"][5*3]
                # tj : 5 * 3 is the x position of the left shoulder
                # = tj : the second [0] for x coordinate
                idx_people = 0
                for j in range(1, num_people):
                    x_left_shoulder_curr = keypoints[i]["people"][j]["pose_keypoints_2d"][5*3]
                    if x_left_shoulder_curr < x_left_shoulder_most_left:
                        x_left_shoulder_most_left = x_left_shoulder_curr
                        idx_people = j
            else:
                idx_people = 0
                # draw_openpose(keypoints[i]["people"][0]["pose_keypoints_2d"])

            skeleton_conf[i, :, :] = extract_skeleton(keypoints[i]["people"][idx_people]["pose_keypoints_2d"])

        skeleton = skeleton_conf[:, :, :2]
        conf = skeleton_conf[:, :, 2]

        self.skeleton[openpose_file] = skeleton
        self.conf[openpose_file] = conf
        self.skeleton_dim = 116 * 2
        i = 0
        while i < num_frames:
            self.samples.append({'openpose_path': openpose_file,
                                 'start_frame': i})
            i += window_stride


        print('1')

    def __len__(self):
        """
        Return the total number of samples
        """
        return len(self.samples)

    def __getitem__(self, index):
        """
        Generate one sample of the dataset
        """
        sample = {}

        sample = self.samples[index]
        openpose_path = sample['openpose_path']
        start_frame = sample['start_frame']
        if openpose_path not in self.skeleton:
            return None

        if self.with_images:
            video_path = openpose_path.replace('OpenPose_r704', 'Videos').replace('openpose.tar.xz', 'mp4')
            vidcap = cv2.VideoCapture(video_path)
            video_res_w = int(vidcap.get(cv2.CAP_PROP_FRAME_WIDTH))
            video_res_h = int(vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT))

            thwc = np.empty([self.window_size, video_res_h, video_res_w, 3], dtype=int)

            vidcap.set(cv2.CAP_PROP_POS_FRAMES, start_frame)
            for i, _ in enumerate(range(self.window_size)):
                success, bgr = vidcap.read()
                # BGR (OpenCV) to RGB (Torch)
                if success:
                    rgb = bgr[:, :, [2, 1, 0]]
                    # chw = np.transpose(rgb, (2, 0, 1))  # C*H*W
                    thwc[i, :, :, :] = rgb

            sample['thwc'] = thwc
        skeleton = self.skeleton[openpose_path][start_frame:start_frame + self.window_size]
        sample['skeleton'] = skeleton # x, y

        sample['name'] = openpose_path.split('/')[-1].split('.')[0]

        return sample
