# coding: utf-8
"""
Module to represents whole models
"""

import numpy as np

import torch
import torch.nn as nn
from torch import Tensor
import torch.nn.functional as F

#from src.initialization import initialize_model
from src.embeddings import Embeddings
from src.encoders import Encoder, TransformerEncoder
from src.batch_with_mask import BatchWithMask, BatchWithMaskConf
from src.initialization import initialize_model

class Model(nn.Module):
    """
    Base Model class
    """

    def __init__(self,
                 encoder: Encoder,
                 embed: Embeddings
                 ) -> None:

        super(Model, self).__init__()

        self.embed = embed
        self.encoder = encoder
        self.skeleton_output_layer1 = nn.Sequential(
            nn.Linear(self.encoder.hidden_size, self.encoder.hidden_size),
            nn.LeakyReLU(0.2))
        self.skeleton_output_layer2 = nn.Sequential(
            nn.Linear(self.encoder.hidden_size, int(self.encoder.hidden_size/2)),
            nn.LeakyReLU(0.2),
            nn.Linear(int(self.encoder.hidden_size/2), self.embed.skeleton_dim),
        )


        # self.cls_output_layer = nn.Linear(hidden_size, 1, bias=False)


    # pylint: disable=arguments-differ

    def forward(self, skeleton: Tensor, train_mask: Tensor = None):

        masked_skeleton = skeleton.clone()
        if train_mask is not None:
            expanded_train_mask = train_mask.unsqueeze(-1).expand_as(skeleton)
            masked_skeleton[expanded_train_mask > 0] = 0


        embed_skeleton = self.embed(x=masked_skeleton)

        encoded_skeleton = self.encoder(embed_src=embed_skeleton)

        tmp1 = self.skeleton_output_layer1(encoded_skeleton)
        tmp2 = encoded_skeleton + tmp1
        outputs = self.skeleton_output_layer2(tmp2)
        batch_size, window_size = outputs.shape[0:2]
        outputs = torch.reshape(outputs, (batch_size, window_size, -1, 2))
        return outputs





    def get_loss_for_batch(self, batch: BatchWithMaskConf, loss_function: nn.Module, with_c):

        # pylint: disable=unused-variable


        skeleton_output, masked_src = self.forward(
            src=batch.skeleton,
            src_mask=batch.mask,
            src_conf=batch.conf,
            with_c=with_c,
            )

        # compute batch loss
        batch_loss = loss_function(skeleton_output, batch.skeleton)
        # return batch loss = sum over all elements in batch that are not pad
        return batch_loss, skeleton_output, masked_src



    def __repr__(self) -> str:
        """
        String representation: a description of encoder, decoder and embeddings

        :return: string representation
        """
        return "%s(\n" \
               "\tencoder=%s,\n" \
               "\tsrc_embed=%s,\n" \
               % (self.__class__.__name__, self.encoder,
                  self.embed)


def build_model(cfg: dict = None,
                skeleton_dim: int = 150) -> Model:
    """
    Build and initialize the model according to the configuration.

    :param cfg: dictionary configuration containing model specifications
    :param src_vocab: source vocabulary
    :param trg_vocab: target vocabulary
    :return: built and initialized model
    """


    embed = Embeddings(
        **cfg["encoder"]["embeddings"], skeleton_dim=skeleton_dim)


    # build encoder
    enc_dropout = cfg["encoder"].get("dropout", 0.)
    enc_emb_dropout = cfg["encoder"]["embeddings"].get("dropout", enc_dropout)

    assert cfg["encoder"]["embeddings"]["embedding_dim"] == \
           cfg["encoder"]["hidden_size"], \
        "for transformer, emb_size must be hidden_size"

    encoder = TransformerEncoder(**cfg["encoder"],
                                     emb_size=embed.embedding_dim,
                                     emb_dropout=enc_emb_dropout)



    model = Model(encoder=encoder, embed=embed)

    # custom initialization of model parameters
    initialize_model(model, cfg)

    return model
