# coding: utf-8
"""
This modules holds methods for generating predictions from a model.
"""
import os
from os.path import *
import sys
from typing import List, Optional
from logging import Logger
import numpy as np


from src.model import build_model

from src.data import *
from util.vizutils import *


def test(cfg_file,
         ckpt:str,
         logger:Logger=None) -> None:

    cfg = load_config(cfg_file)

    set_seed(seed=cfg["training"].get("random_seed", 42))

    test_batch_size = cfg['testing']['batch_size']

    test_dir = cfg['testing']['test_dir']


    test_data = load_test_data(data_cfg=cfg["data"])


    model = build_model(cfg["model"], test_data.skeleton_dim)  # tj : build our model
    model = model.cuda()

    # model = torch.nn.DataParallel(model).cuda()



    checkpoint = torch.load(ckpt)
    model.load_state_dict(checkpoint["model_state"])
    model.eval()

    test_loader = make_data_loader(dataset=test_data,
                                         batch_size=test_batch_size,
                                         shuffle=True)

    rgb_win, input_win, masked_input_win, output_win, fig = None, None, None, None, None
    for i, batch in enumerate(test_loader):
        if batch is None:  # tj : filter out None in my_collate
            continue

        with torch.no_grad():

            skeleton = batch['skeleton']

            joint_mask = batch['joint_mask']
            # train_mask = batch['train_mask']



            masked_skeleton = skeleton.clone()

            expanded_train_mask = joint_mask.unsqueeze(-1).expand_as(skeleton)
            masked_skeleton[expanded_train_mask == 0] = 0


            skeleton_cuda = skeleton.cuda()

            outputs_cuda = model(skeleton_cuda, train_mask=None)



            # num_figs = 20
            # if is_show(num_figs, i, len(valid_loader)):
            fname = "%s%05d" % ('eval_', i)  # tj : [0] to get the frist sample in the mini-batch
            save_fig_dir = os.path.join(test_dir, "figs")
            save_path = os.path.join(save_fig_dir, fname)

            rgb_win, input_win, masked_input_win, output_win, fig = viz_gt_pred(
                batch['thwc'][0].numpy(),
                skeleton[0].numpy(),
                masked_skeleton[0].numpy(),
                outputs_cuda.cpu()[0].numpy(),
                rgb_win,
                input_win,
                masked_input_win,
                output_win,
                fig,
                save_path=save_path,
                show=False,
            )
            print()
