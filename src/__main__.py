import argparse
from src.training import train
from src.helpers import *
from src.test import *
from src.eval import *
# tj = :  command : python3 -m src train ./config.yaml
def main():
    ap = argparse.ArgumentParser("BERT Skeleton")

    ap.add_argument("--mode", choices=["train", "test", "eval"], default="train",
                    help="train a model")

    ap.add_argument("--config_path", type=str, default="../config.yaml",
                    help="path to YAML config file")

    ap.add_argument("--ckpt", type=str,
                    help="checkpoint for prediction")

    ap.add_argument("--seq_name", type=str)

    ap.add_argument("--sub_dir", type=str)

    ap.add_argument("--output_dir", type=str)

    args = ap.parse_args()

    if args.mode == "train":
        train(cfg_file=args.config_path)
    elif args.mode == "test":
        test(cfg_file=args.config_path, ckpt=args.ckpt)
    elif args.mode == "eval":
        evaluate(cfg_file=args.config_path, ckpt=args.ckpt)
    else:
        raise ValueError("unknown mode")

def load_data():
    a = load_gklz('/home/seamanj/Workspace/OpenPose_Skeletor/eval/exp1/skeleton/Michaela_Signing_I.gklz')
    print('1')

if __name__ == "__main__":

    # load_data()
    main()