import torch
from src.helpers import load_config, set_seed, make_model_dir, make_logger,\
    symlink_update, load_checkpoint, ConfigurationError, log_cfg, append_file
from src.data import  make_data_loader, load_train_data, load_val_data, load_test_data
from torch.utils import data
from src.model import build_model
from src.model import Model
from torch.utils.tensorboard import SummaryWriter
from src.builders import build_gradient_clipper, build_optimizer, build_scheduler
import numpy as np
import os
from torch.utils.data import Dataset
import time
from src.batch_with_mask import BatchWithMask, BatchWithMaskConf
from torch import Tensor
import queue
import shutil
from src.mask import gen_mask, gen_mask_option, gen_joint_mask_option
from src.loss import Skeletor_Loss
from src.averagemeter import *

class TrainManager:
    def __init__(self, model: Model, config: dict) -> None:
        train_config = config["training"]
        data_config = config["data"]
        self.train_config = train_config
        self.data_config = data_config

        self.window_size = data_config["window_size"]
        self.save_last = train_config['save_last']
        self.model_dir, init = make_model_dir(train_config["model_dir"],
                                        overwrite=train_config["overwrite"])
        self.logger = make_logger("{}/train.log".format(self.model_dir))
        self.logging_freq = train_config.get("logging_freq", 100)
        self.valid_log_file = "{}/validations.log".format(self.model_dir)
        self.valid_report_file = "{}/validations.txt".format(self.model_dir)
        with open(self.valid_report_file, 'a') as opened_file:
            opened_file.write(
                "Epoch\tSteps\ttrain_Loss\t"
                "val_loss\tLR\n")

        self.tb_writer = SummaryWriter(log_dir=self.model_dir + "/tensorboard/")

        self.with_confidence = train_config.get('with_confidence', False)
        self.with_sequence_ordering = data_config.get('with_sequence_ordering', False)
        #objective]
        self.loss = Skeletor_Loss()

        # model
        self.model = model
        self._log_parameters_list()

        #optimization
        self.learning_rate_min = train_config.get("learning_rate_min", 1.0e-8)
        self.clip_grad_fun = build_gradient_clipper(config=train_config)
        self.optimizer = build_optimizer(config=train_config,
                                         parameters=model.parameters())

        # validation & early stopping
        self.validation_freq = train_config.get("validation_freq", 1000)
        self.max_validation_batches = train_config.get("max_validation_batches", 0)
        self.ckpt_queue = queue.Queue(
            maxsize=train_config.get("keep_last_ckpts", 5))
        self.eval_metric = train_config.get("eval_metric", "bleu")

        self.early_stopping_metric = train_config.get("early_stopping_metric",
                                                      "loss")

        # if we schedule after BLEU/chrf, we want to maximize it, else minimize
        # early_stopping_metric decides on how to find the early stopping point:
        # ckpts are written when there's a new high/low score for this metric
        if self.early_stopping_metric in ["ppl", "loss"]:
            self.minimize_metric = True
        else:
            raise ConfigurationError(
                "Invalid setting for 'early_stopping_metric', "
                "valid options: 'loss', 'ppl'.")

        # learning rate sheduling
        self.scheduler, self.scheduler_step_at = build_scheduler(
            config=train_config,
            scheduler_mode="min" if self.minimize_metric else "max",
            optimizer=self.optimizer,
            hidden_size=config["model"]["encoder"]["hidden_size"])

        # data & batch handling
        self.shuffle = train_config.get("shuffle", True)
        self.max_epoch = train_config["max_epoch"]
        self.batch_size = train_config["batch_size"]
        self.eval_batch_size = train_config.get("eval_batch_size",
                                                self.batch_size)
        self.batch_multiplier = train_config.get("batch_multiplier", 1)

        self.mask_percentage = train_config.get("mask_percentage", 0.15)
        self.eval_mask_percentage = train_config["eval_mask_percentage"]
        self.masking_times = train_config.get("masking_times", 100)

        # CPU / GPU
        self.use_cuda = train_config["use_cuda"]
        if self.use_cuda:
            self.model.cuda()
            # self.loss.cuda()

        self.mse_loss = nn.MSELoss(reduction='none')

        # initialize training statistics
        self.steps = 0
        # stop training if this flag is True by reaching learning rate minimum
        self.stop = False
        self.epoch = 0

        self.best_ckpt_iteration = 0
        # initial values for best scores
        self.best_ckpt_score = np.inf if self.minimize_metric else -np.inf
        # comparison function for scores
        self.is_best = lambda score: score < self.best_ckpt_score \
            if self.minimize_metric else score > self.best_ckpt_score

        if init is True:  # tj : init from last exit
            last_model = os.path.join(self.model_dir, "last.ckpt")
            best_model = os.path.join(self.model_dir, "best.ckpt")
            if os.path.exists(last_model):
                self.logger.info("init from last checkpoint {}".format(last_model))
                self.init_from_checkpoint(last_model)
            elif os.path.exists(best_model):
                self.logger.info("init from best checkpoint {}".format(best_model))
                self.init_from_checkpoint(best_model)
            else:
                print("no last or best model found! overwrite now")
                make_model_dir(train_config["model_dir"], overwrite=True)
                self._init_from_scratch()
        else:  # tj : check shall we start based on other pre-trained model
            self._init_from_scratch()




    def _init_from_scratch(self):
        if "load_model" in self.train_config.keys():
            model_load_path = self.train_config["load_model"]
            self.logger.info("Loading model from %s", model_load_path)
            reset_epoch = self.train_config.get("reset_epoch", False)
            reset_steps = self.train_config.get("reset_steps", False)
            reset_best_ckpt = self.train_config.get("reset_best_ckpt", False)
            reset_scheduler = self.train_config.get("reset_scheduler", False)
            reset_optimizer = self.train_config.get("reset_optimizer", False)
            self.init_from_checkpoint(model_load_path,
                                      reset_epoch=reset_epoch,
                                      reset_steps=reset_steps,
                                      reset_best_ckpt=reset_best_ckpt,
                                      reset_scheduler=reset_scheduler,
                                      reset_optimizer=reset_optimizer)


    def _save_checkpoint(self) -> None:
        model_path = "{}/{}.ckpt".format(self.model_dir, self.steps)
        state = {
            "epoch": self.epoch,
            "steps": self.steps,
            "best_ckpt_score": self.best_ckpt_score,
            "best_ckpt_iteration": self.best_ckpt_iteration,
            "model_state": self.model.state_dict(),
            "optimizer_state": self.optimizer.state_dict(),
            "scheduler_state": self.scheduler.state_dict() if \
            self.scheduler is not None else None,
        }
        torch.save(state, model_path)
        if self.ckpt_queue.full():
            to_delete = self.ckpt_queue.get()  # delete oldest ckpt
            try:
                os.remove(to_delete)
            except FileNotFoundError:
                self.logger.warning("Wanted to delete old checkpoint %s but "
                                    "file does not exist.", to_delete)

        self.ckpt_queue.put(model_path)

        best_path = "{}/best.ckpt".format(self.model_dir)
        try:
            # create/modify symbolic link for best checkpoint
            symlink_update("{}.ckpt".format(self.steps), best_path)
        except OSError:
            # overwrite best.ckpt
            torch.save(state, best_path)

    def _save_last_checkpoint(self) -> None:
        model_path = "{}/last.ckpt".format(self.model_dir)
        state = {
            "epoch": self.epoch,
            "steps": self.steps,
            "best_ckpt_score": self.best_ckpt_score,
            "best_ckpt_iteration": self.best_ckpt_iteration,
            "model_state": self.model.state_dict(),
            "optimizer_state": self.optimizer.state_dict(),
            "scheduler_state": self.scheduler.state_dict() if \
            self.scheduler is not None else None,
        }
        torch.save(state, model_path)

    def init_from_checkpoint(self, path: str,
                             reset_epoch: bool = False,
                             reset_steps: bool = False,
                             reset_best_ckpt: bool = False,
                             reset_scheduler: bool = False,
                             reset_optimizer: bool = False) -> None:

        model_checkpoint = load_checkpoint(path=path, use_cuda=self.use_cuda)
        self.model.load_state_dict(model_checkpoint['model_state'])


        if not reset_optimizer:
            self.optimizer.load_state_dict(model_checkpoint["optimizer_state"])
        else:
            self.logger.info("Reset optimizer.")

        if not reset_scheduler:
            if model_checkpoint["scheduler_state"] is not None and \
                    self.scheduler is not None:
                self.scheduler.load_state_dict(
                    model_checkpoint["scheduler_state"])
        else:
            self.logger.info("Reset scheduler.")

        # restore counts

        if not reset_epoch:
            self.epoch = model_checkpoint["epoch"] + 1
        else:
            self.logger.info("Reset epoch.")

        if not reset_steps:
            self.steps = model_checkpoint["steps"]
        else:
            self.logger.info("Reset steps.")

        if not reset_best_ckpt:
            self.best_ckpt_score = model_checkpoint["best_ckpt_score"]
            self.best_ckpt_iteration = model_checkpoint["best_ckpt_iteration"]
        else:
            self.logger.info("Reset tracking of the best checkpoint.")

        # move parameters to cuda
        if self.use_cuda:
            self.model.cuda()



    def _log_parameters_list(self) -> None:
        model_parameters = filter(lambda p: p.requires_grad,
                                  self.model.parameters())
        n_params = sum([np.prod(p.size()) for p in model_parameters])
        self.logger.info("Total params: %d", n_params)
        trainable_params = [n for (n, p) in self.model.named_parameters()
                            if p.requires_grad]
        self.logger.info("Trainable prameters: %s", sorted(trainable_params))
        assert trainable_params


    def do_epoch(self, set_name):
        assert set_name == 'train' or set_name == 'val'
        losses = [AverageMeter()]
        # perfs = []
        F1 = F1Meter()
        # for k in self.topk:
        #     perfs.append(AverageMeter())

        loader = None
        if set_name == 'train':
            self.model.train()
            loader = self.train_loader
        elif set_name == 'val':
            self.model.eval()
            loader = self.val_loader
        win, fig_gt_pred = None, None
        for i, batch in enumerate(loader):
            if batch is None:  # tj : filter out None in my_collate
                continue
            # with torch.no_grad():
            #     batch["rgb"] = batch["rgb"].cuda()
            #     collater_kwargs = {}
            #     collater = loader.dataset.gpu_collater
            #     batch = collater(minibatch=batch, **collater_kwargs)  # tj: 256 -> 224

            skeleton = batch['skeleton']
            joint_mask = batch['joint_mask']
            train_mask = batch['train_mask']


            skeleton_cuda = skeleton.cuda()
            train_mask_cuda = train_mask.cuda()

            expanded_joint_mask = joint_mask.unsqueeze(-1).expand_as(skeleton)

            outputs_cuda = self.model(skeleton_cuda, train_mask_cuda)

            batch_size = skeleton_cuda.shape[0]

            # conpute the loss
            loss = self.mse_loss(outputs_cuda, skeleton_cuda)
            masked_loss = (loss.cpu() * expanded_joint_mask).sum()

            num_nonzero = expanded_joint_mask.sum()

            losses[0].update(masked_loss.item() / num_nonzero, num_nonzero)

            if set_name == 'train': # tj : # log training progress
                if self.scheduler is not None and \
                        self.scheduler_step_at == "step":
                    self.scheduler.step()

                if self.steps % self.logging_freq == 0:
                    self.logger.info(
                        "Epoch %3d Step: %8d Batch Loss: %12.6f Lr: %12.8f",
                        self.epoch, self.steps, float(losses[0].avg), self.optimizer.param_groups[0]["lr"])

            if set_name == 'train':
                self.optimizer.zero_grad()
                masked_loss.backward()
                self.optimizer.step()

                if self.clip_grad_fun is not None:
                    # clip gradients (in-place)
                    self.clip_grad_fun(params=self.model.parameters())

                self.steps += 1

        return losses

    def train_and_validate(self, train_data: Dataset, val_data: Dataset) -> None:
        self.train_loader = make_data_loader(train_data,
                                    batch_size=self.batch_size,
                                    shuffle=self.shuffle)
        self.val_loader = make_data_loader(val_data,
                                    batch_size=self.batch_size,
                                    shuffle=False)
        # for epoch_no in range(self.epochs):
        while self.epoch < self.max_epoch:
            self.logger.info("EPOCH %d", self.epoch)

            if self.scheduler is not None and self.scheduler_step_at == "epoch":
                self.scheduler.step(epoch=self.epoch)

            train_losses = self.do_epoch('train')
            with torch.no_grad():
                val_losses = self.do_epoch('val')

            # tj : log

            train_loss = float(train_losses[0].avg)  # tj : only one loss

            valid_loss = float(val_losses[0].avg)

            self.tb_writer.add_scalar("train/loss", train_loss, self.epoch)

            self.tb_writer.add_scalar("val/loss", valid_loss, self.epoch)

            # tj : save checkpoint
            if self.early_stopping_metric == "loss":
                ckpt_score = valid_loss
            else:
                raise ConfigurationError("Nothing else supported yet!")

            new_best = False
            if self.is_best(ckpt_score):
                self.best_ckpt_score = ckpt_score
                self.best_ckpt_iteration = self.steps
                self.logger.info(
                    'Hooray! New best validation result [%s]!',
                    self.early_stopping_metric)
                if self.ckpt_queue.maxsize > 0:
                    self.logger.info("Saving new checkpoint.")
                    new_best = True
                    self._save_checkpoint()

            if self.save_last:
                self.logger.info("Saving the last checkpoint")
                self._save_last_checkpoint()

            # tj : as we validate every epoch here, so scheduler_step_at == 'validation' equals to scheduler_step_at == 'epoch'
            if self.scheduler is not None \
                    and self.scheduler_step_at == "validation":
                self.scheduler.step(ckpt_score)

            self._add_report(
                train_loss=train_loss,
                # train_F1B=train_F1B,
                # train_top5=train_top5,
                val_loss=valid_loss,
                # val_F1B=valid_F1B,
                # val_top5=valid_top5,
                new_best=new_best)

            # tj : check whether to stop or not
            if self.stop:
                self.logger.info(
                    'Training ended since minimum lr %12.8f was reached.',
                    self.learning_rate_min
                )
                self.logger.info('Training ended after %3d epochs.', self.epoch)
                break

            self.epoch += 1

        self.tb_writer.close()
        # self.logger.info("EPOCH %d", self.epoch)
        #
        # if self.scheduler is not None and self.scheduler_step_at == "epoch":
        #     self.scheduler.step(epoch=self.epoch)
        #
        #     train_losses, train_F1 = self.do_epoch('train')
        #     with torch.no_grad():
        #         val_losses, val_F1 = self.do_epoch('val')
        #
        #     self.model.train()
        #
        #     start = time.time()
        #     total_valid_duration = 0
        #     count = self.batch_multiplier - 1
        #     epoch_loss = 0
        #
        #     for batch in train_loader:
        #         self.model.train()
        #         real_batch_size = batch['skeleton'].shape[0]
        #         for i in range(self.masking_times):
        #
        #             mask = gen_joint_mask_option(real_batch_size, self.window_size, self.mask_percentage, batch['confidence'],\
        #                              option='highest')
        #             batch_with_mask_conf = BatchWithMaskConf(batch['skeleton'],\
        #                                                 mask, batch['confidence'],
        #                                                  use_cuda=self.use_cuda)
        #
        #             update = count == 0
        #             batch_loss = self._train_batch(batch_with_mask_conf, update=update)
        #             self.tb_writer.add_scalar("train/train_batch_loss", batch_loss,
        #                                   self.steps)
        #             count = self.batch_multiplier if update else count
        #             count -= 1
        #             epoch_loss += batch_loss.detach().cpu().numpy()
        #             if self.scheduler is not None and \
        #                     self.scheduler_step_at == "step" and update:
        #                 self.scheduler.step()
        #
        #             # log learning progress
        #             if self.steps % self.logging_freq == 0 and update:
        #                 elapsed = time.time() - start - total_valid_duration
        #                 self.logger.info(
        #                     "Epoch %3d Step: %8d Batch Loss: %12.6f Lr: %12.8f",
        #                     epoch_no + 1, self.steps, batch_loss,
        #                     self.optimizer.param_groups[0]["lr"])
        #                 start = time.time()
        #                 total_valid_duration = 0
        #
        #             # validate on the entire dev set
        #             if self.steps % self.validation_freq == 0 and update:
        #                 valid_start_time = time.time()
        #
        #                 valid_loss, _ = validate_on_data(
        #                         batch_size=self.eval_batch_size,
        #                         max_validation_batches = self.max_validation_batches,
        #                         data=val_data,
        #                         window_size=self.window_size,
        #                         mask_percentage=self.eval_mask_percentage,
        #                         model=self.model,
        #                         use_cuda=self.use_cuda,
        #                         loss_function=self.loss,
        #                         log_filename=self.valid_log_file,
        #                         with_confidence=self.with_confidence,
        #                     )
        #
        #                 self.tb_writer.add_scalar("valid/valid_loss",
        #                                           valid_loss, self.steps)
        #
        #                 if self.early_stopping_metric == "loss":
        #                     ckpt_score = valid_loss
        #                 else:
        #                     raise ConfigurationError("Nothing else supported yet!")
        #
        #                 new_best = False
        #
        #                 if self.is_best(ckpt_score):
        #                     self.best_ckpt_score = ckpt_score
        #                     self.best_ckpt_iteration = self.steps
        #                     self.logger.info(
        #                         'Hooray! New best validation result [%s]!',
        #                         self.early_stopping_metric)
        #                     if self.ckpt_queue.maxsize > 0:
        #                         self.logger.info("Saving new checkpoint.")
        #                         new_best = True
        #                         self._save_checkpoint()
        #                 # else: # save the last state
        #                 #     self.best_ckpt_score = ckpt_score
        #                 #     self.best_ckpt_iteration = self.steps
        #                 #     # the last checkpoint is saved
        #                 #     self.logger.info("Saving the last checkpoint.")
        #                 #     self._save_last_checkpoint()
        #
        #                 if self.scheduler is not None \
        #                         and self.scheduler_step_at == "validation":
        #                     self.scheduler.step(ckpt_score)
        #
        #                 # append to validation report
        #                 self._add_report(
        #                     valid_loss=valid_loss,
        #                     new_best=new_best)
        #                 # = tj : in this funciton, if current_lr < self.learning_rate_min: self.stop = True
        #
        #                 valid_duration = time.time() - valid_start_time
        #                 total_valid_duration += valid_duration
        #                 self.logger.info(
        #                     'Validation result at epoch %3d, '
        #                     'step %8d: loss: %8.6f '
        #                     'duration: %.4fs', epoch_no + 1, self.steps,
        #                     valid_loss,
        #                     valid_duration)
        #
        #                 append_file(self.valid_log_file, 'Validation result at epoch %3d, '
        #                                                  'step %8d: loss: %8.6f, duration: %.4fs\n'
        #                             %(epoch_no + 1, self.steps, valid_loss, valid_duration))
        #
        #             if self.stop:
        #                 break
        #         if self.stop:
        #             break
        #     if self.stop:
        #         self.logger.info(
        #             'Training ended since minimum lr %12.8f was reached.',
        #             self.learning_rate_min
        #         )
        #         # break
        #     self.logger.info('Epoch %3d: total training loss %.2f', epoch_no + 1,
        #                      epoch_loss)
        # else:
        #     self.logger.info('Training ended after %3d epochs.', epoch_no+1)
        #
        # self.tb_writer.close()  # close Tensorboard writer

    def _train_batch(self, batch: BatchWithMaskConf, update: bool = True) -> (Tensor, Tensor, Tensor):

        batch_loss,_,_ = self.model.get_loss_for_batch(
            batch=batch, loss_function=self.loss, with_c=self.with_confidence)


        # division needed since loss.backward sums the gradients until updated
        norm_batch_multiply = batch_loss / self.batch_multiplier

        norm_batch_multiply.backward()

        if self.clip_grad_fun is not None:
            # clip gradients (in-place)
            self.clip_grad_fun(params=self.model.parameters())

        if update:
            self.optimizer.step()
            self.optimizer.zero_grad()

            # increment step counter
            self.steps += 1

        return batch_loss



    def _add_report(self, train_loss: float, val_loss: float, new_best: bool = False) -> None:
        """
        Append a one-line report to validation logging file.

        :param valid_score: validation evaluation score [eval_metric]
        :param valid_loss: validation loss (sum over whole validation set)
        :param eval_metric: evaluation metric, e.g. "bleu"
        :param new_best: whether this is a new best model
        """
        current_lr = -1
        # ignores other param groups for now
        for param_group in self.optimizer.param_groups:
            current_lr = param_group['lr']

        if current_lr < self.learning_rate_min:
            self.stop = True

        with open(self.valid_report_file, 'a') as opened_file:
            opened_file.write(
                "{:<5}\t{:<5}\t{:<10.3f}\t"
                "{:<8.3f}\t{:.8f}\t{}\n".format(
                    self.epoch, self.steps, train_loss,
                    val_loss, current_lr, "*" if new_best else ""))





def train(cfg_file: str) -> None:

    cfg = load_config(cfg_file)

    set_seed(seed=cfg["training"].get("random_seed", 42))

    #training_data, dev_data, test_data = load_data(data_cfg=cfg["data"])

    train_data = load_train_data(data_cfg=cfg["data"])
    val_data = load_val_data(data_cfg=cfg["data"])


    model = build_model(cfg["model"], train_data.skeleton_dim)

    trainer = TrainManager(model=model, config=cfg)  # tj : all the setting pass to its member

    trainer.logger.info("train frames:{}".format(train_data.__len__()))
    trainer.logger.info("dev frames:{}".format(val_data.__len__()))


    shutil.copy2(cfg_file, trainer.model_dir + "/config.yaml")

    # log all entries of config
    log_cfg(cfg, trainer.logger)

    #TODO : log data info

    trainer.logger.info(str(model))

    trainer.train_and_validate(train_data=train_data, val_data=val_data)

