#!/bin/bash

cd /vol/research/SignPose/tj/Workspace/OpenPose_Skeletor/exp1
/vol/research/SignPose/tj/Software/miniconda3/envs/BERT_skeleton/bin/python -m src --mode=train --config_path=./configs/exp1/config.yaml